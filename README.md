# voyc

CommodoreBASIC compiler that targets the HP-15C calculator and possibly other voyager series calculators later.

It can compile "Hammurabi", a game from 1968: <a href="https://www.atariarchives.org/basicgames/showpage.php?page=79">Hammurabi BASIC</a> gets compiled to <a href="https://forum.swissmicros.com/viewtopic.php?f=23&t=2193">Hammurabi HP 15C</a> without any changes in the input source code. Also see this <a href="https://pastebin.com/svGJk9Mh">more detailed compiler output</a>.

Many features are currently missing, e.g. arrays and functions. The code is a mess since this is the author's first Idris program (It might make sense to rewrite this in Clojure).

## Build the Compiler

You can compile it like this:

1. Get the haskell-platform
2. Install idris
3. Start an idris REPL inside of the src folder
4. idris --build voyc.ipkg or make build

## Compile a BASIC Program

  ./voyc example.bas >output.txt

Note that this outputs two things: The code and the string table. You should print the string table on a piece of paper, the calculator will show complex numbers to refer to strings.

## Use on the Calculator

1. Start with "B"
2. Continue with R/S when the calculator stops

| Calculator shows | Means | Do |
| :--------------: | :---: | :---: |
|   Number         | The number shown | Press R/S to continue |
| Complex number | The text with that number | Look up the text, then press R/S to continue |
|   Blinking  | Input required | Enter a number, then press R/S to continue (you don't have to press ENTER) |

## License

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

