module ParserCombinators.Urbit

import ParserCombinators.Parsers


%access public export

pal : Parser String
pal = literal "("

par : Parser String
par = literal ")"

tis : Parser String
tis = literal "="

gal : Parser String
gal = literal "<"

gar : Parser String
gar = literal ">"

lus : Parser String
lus = literal "+"

hep : Parser String
hep = literal "-"

tar : Parser String
tar = literal "*"

fas : Parser String
fas = literal "/"

bas : Parser String
bas = literal "\\"

col : Parser String
col = literal ":"

mic : Parser String
mic = literal ";"

com : Parser String
com = literal ","

dot : Parser String
dot = literal "."

doq : Parser String
doq = literal "\""

soq : Parser String
soq = literal "'"

hax : Parser String
hax = literal "#"

bar : Parser String
bar = literal "|"

kel : Parser String
kel = literal "{"

ker : Parser String
ker = literal "}"

buc : Parser String
buc = literal "$"

cen : Parser String
cen = literal "%"

pam : Parser String
pam = literal "&"

pat : Parser String
pat = literal "@"

ket : Parser String
ket = literal "^"

cab : Parser String
cab = literal "_"

sel : Parser String
sel = literal "["

ser : Parser String
ser = literal "]"

zap : Parser String
zap = literal "!"

wut : Parser String
wut = literal "?"

tic : Parser String
tic = literal "`"

sig : Parser String
sig = literal "~"

-- multiple at once

galgar : Parser String
galgar = literal "<>"

galtis : Parser String
galtis = literal "<="

gargal : Parser String
gargal = literal "><"

gartis : Parser String
gartis = literal ">="

colcoltis : Parser String
colcoltis = literal "::="
