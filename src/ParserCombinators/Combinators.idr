module ParserCombinators.Combinators

import Data.Vect

%access public export
%default total


---------------------------------
-- Data    ----------------------
---------------------------------

ParseResult : Type -> Type
ParseResult parseResult = Either String (String, parseResult)

parseFailure : String -> ParseResult parseResult
parseFailure s = Left s

parseSuccess : String -> parseResult -> ParseResult parseResult
parseSuccess s parseResult = Right (s, parseResult)


data Parser parseResult = MkParser (String -> ParseResult parseResult)
implementation Functor Parser where
  map f (MkParser oldP) = MkParser (\s => pushin f (oldP s)) where
    pushin : (f : a -> b) -> ParseResult a -> ParseResult b
    pushin f r = case r of
                  (Left s) => Left s
                  (Right (s, e)) => Right (s, f e)

dropList : Nat -> List a -> List a
dropList Z la = la
dropList (S k) [] = []
dropList (S k) (a :: as) = drop k as

drop : Nat -> String -> String
drop Z s = s
drop (S k) "" = ""
drop n s = pack $ dropList n $ unpack s

smellyStrTail : String -> String
smellyStrTail = drop 1

-- fake total, lies for empty strings
smellyStrHead : String -> Char
smellyStrHead s = fromMaybe '?' $ head' $ unpack s -- bogus '?'

literal : String -> Parser String
literal expected = MkParser (\s => if isPrefixOf expected s
                                     then parseSuccess (drop (length expected) s) expected
                                     else parseFailure s)

parse : Parser a -> String -> ParseResult a
parse (MkParser f) = f



maybeStrHead : String -> Maybe Char
maybeStrHead "" = Nothing
maybeStrHead s = head' $ unpack s

--------------------
-- Combinators -----
--------------------

-- It's partial because (zeroOrMore (literal "")) results in an infinite loop
partial
zeroOrMore : Parser a -> Parser (List a)
zeroOrMore p = MkParser $ Right . (\s => case parse p s of
                      (Right (rest, a)) => case parse (zeroOrMore p) rest of
                                              (Right (rest2, as)) => (rest2, (a :: as))
                                              (Left rest2) => (rest2, [a]) -- can't happen?
                      (Left rest) => (rest, []))

-- Should be Parser a -> Vect (S k) a ?
partial
oneOrMore : Parser a -> Parser (List a)
oneOrMore p = MkParser (\s => case parse p s of
                     (Right (rest, a)) => case parse (zeroOrMore p) rest of
                                             (Right (rest2, as)) => parseSuccess rest2 (a :: as)
                                             (Left rest2) => parseSuccess rest2 [a] -- can't happen?
                     (Left all) => parseFailure all)

(>>=) : Parser a -> (a -> Parser b) -> Parser b
p >>= f = MkParser (\s => let r1 = parse p s in case r1 of
                             Right (nextInput, result) => parse (f result) nextInput
                             Left fail => Left fail)

pair : Parser a -> Parser b -> Parser (a, b)
pair p1 p2 = MkParser (\s1 => let r1 = parse p1 s1 in
                      case r1 of
                           Right (nextInput1, e1) => let r2 = parse p2 nextInput1 in
                                                         case r2 of
                                                              Right (nextInput2, e2) => Right (nextInput2, (e1, e2))
                                                              Left _ => Left s1 -- <- IMPORTANT, or you'll lose input text at the end of *OrMore
                           Left _ => Left s1)

{- WRONG
pair p1 p2 = MkParser (\s1 => do
  (s2, r1) <- parse p1 s1
  (s3, r2) <- parse p2 s2
  pure (s3, (r1, r2)))
  -}
{- ALSO WRONG?
pair : Parser a -> Parser b -> Parser (a, b)
pair p1 p2 = p1 >>= (\r1 => (\r2 => (r1, r2)) <$> p2)
-}

pairStr : Parser String -> Parser String -> Parser String
pairStr p1 p2 = (\(a,b) => a ++ b) <$> pair p1 p2

left : Parser a -> Parser b -> Parser a
left p1 p2 = map fst $ pair p1 p2

right : Parser a -> Parser b -> Parser b
right p1 p2 = map snd $ pair p1 p2

middle : Parser l -> Parser m -> Parser r -> Parser m
middle l m r = (right l (left m r))

either : Parser a -> Parser a -> Parser a
either p1 p2 = MkParser (\s => let r1 = parse p1 s in case r1 of
                                  Right _ => r1
                                  Left _ => parse p2 s)

eithers : Vect (S k) (Parser a) -> Parser a
eithers (p :: []) = p
eithers (p :: ps@(ps1 :: psrest)) = either p (eithers ps)

maybe : Parser a -> Parser (Maybe a)
maybe pa =  either (Just <$> pa) (MkParser (\s => Right (s,Nothing)))



predicate : (a -> Bool) -> Parser a -> Parser a
predicate f pa = MkParser (\s => let r = parse pa s in
  case r of
    Right (rest, a) => if f a then r else Left s
    Left fail => r)


