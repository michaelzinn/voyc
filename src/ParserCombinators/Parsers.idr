module ParserCombinators.Parsers

import public ParserCombinators.Combinators
import Data.Vect

%access export
%default total

{-
drop : Nat -> String -> String
drop Z s = s
drop (S k) s = drop k (strTail s)

literal : String -> Parser String
literal expected = MkParser (\s => if isPrefixOf expected s
                             then parseSuccess (drop (length expected) s) expected
                             else parseFailure s)
                             -}

anyChar : Parser Char
anyChar = MkParser (\s => case maybeStrHead s of
                   Just c => Right (smellyStrTail s, c)
                   Nothing => Left "")

-- Useful for parsing comments to the end of the line?
anyString : Parser String
anyString = pack <$> (assert_total $ zeroOrMore anyChar)

anyStringNoLF : Parser String
anyStringNoLF = pack <$> (assert_total $ zeroOrMore $ predicate (/= '\n') anyChar)

-- nat : Parser Nat
-- nat : (the Nat . cast) <$> digits1


isAlphaNumOrDash : Char -> Bool
isAlphaNumOrDash c = isAlphaNum c || ('-' == c)

identifier : Parser String
identifier = MkParser (\s => case s of
                                  "" => parseFailure ""
                                  _ =>  if isAlpha (smellyStrHead s)
                                           then let (parsed, rest) = span isAlphaNumOrDash s in
                                                    parseSuccess rest parsed 
                                           else parseFailure s)

matchIs : (Char -> Bool) -> Parser Char
matchIs cb = MkParser (\s => if s == "" then parseFailure "" else if cb (smellyStrHead s)
                 then parseSuccess (smellyStrTail s) (smellyStrHead s)
                 else parseFailure s)

digit : Parser Char
digit = matchIs isDigit

digits1 : Parser String
digits1 = pack <$> (assert_total $ oneOrMore digit)

nat : Parser Nat
nat = cast <$> digits1

whitespaceChar : Parser Char
whitespaceChar = predicate isSpace anyChar

whitespaceCharNoLF : Parser Char
whitespaceCharNoLF = predicate (\c => (c /= '\n') && isSpace c) anyChar

space1 : Parser (List Char)
space1 = assert_total $ oneOrMore whitespaceCharNoLF

space0 : Parser (List Char)
space0 = assert_total $ zeroOrMore whitespaceCharNoLF

quotedString : Parser String
quotedString = (map pack (middle 
                 (literal "\"") 
                 (assert_total (zeroOrMore (predicate (/= '\"') anyChar)))
                 (literal "\"")))


