module FileIO

import Data.Vect

%access public export

getLines : IO (List String)
getLines = do line <- getLine 
              if line == ""
                 then pure []
                 else (line ::) <$> getLines

-- Reduce function for Strings only, because the Monoid "" is known.
reduceStr : (String -> String -> String) -> List String -> String
reduceStr f [] = ""
reduceStr f (x :: xs) = foldl f x xs

intercalateStr : String -> List String -> String
intercalateStr joiner strings = reduceStr (\l,r => l ++ joiner ++ r) strings

removeCRs : String -> String
removeCRs s = pack $ filter (/= '\r') $ unpack s

fetchLines : (file : File) -> IO (Either FileError (List String))
fetchLines file = do eof <- fEOF file
                     if eof
                        then pure (Right [])
                        else do Right line <- fGetLine file | Left err => pure (Left err)
                                Right lines <- (fetchLines file) | Left err => pure (Left err)
                                pure $ Right (removeCRs line :: lines)


{-
do fhandle <- openFile filename Read 


                    case fhandle of
                         Left fileError => pure (Left fileError)
                         Right file => (map (intercalateStr "\n")) <$> fetchLines file 
                         -}

slurpLines : String -> IO (Either FileError (List String))
slurpLines filename = do fhandle <- openFile filename Read 
                         case fhandle of
                              Left fileError => pure (Left fileError)
                              Right file => fetchLines file 

slurpStr : String -> IO (Either FileError String)
slurpStr filename = (map (intercalateStr "")) <$> slurpLines filename
  
listToVect : List a -> (n : Nat ** Vect n a)
listToVect [] = (0 ** [])
listToVect (x :: xs) = let (len ** vec) = listToVect xs in
                           (_ ** (x :: vec))
                    
readVectFromFile : String -> IO (n ** Vect n String)
readVectFromFile filename = do Right strings <- slurpLines filename | err => pure (0 ** [])
                               pure $ listToVect strings

                          

readToBlank : IO (List String)
readToBlank = getLines


readAndSave : IO ()
readAndSave = do lines <- getLines
                 filename <- getLine
                 Right () <- writeFile filename (intercalateStr "\n" lines)
                   | Left err => putStrLn (show err)
                 pure ()

