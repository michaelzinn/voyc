module Parse.Utils

import public Data.Vect
-- every first project in any language needs a utils dump

%access public export
%default total

uncurry3 : (p1 -> p2 -> p3 -> b) -> (p1, p2, p3) -> b
uncurry3 f (a,b,c) = f a b c

uncurry4 : (p1 -> p2 -> p3 -> p4 -> b) -> (p1, p2, p3, p4) -> b
uncurry4 f (a,b,c,d) = f a b c d

vectToList : Vect n a -> List a
vectToList [] = []
vectToList (x :: xs) = x :: vectToList xs

joinWithList : String -> List String -> String
joinWithList _ [] = ""
joinWithList _ (x :: []) = x
joinWithList j (x :: xs) = x ++ j ++ (joinWithList j xs)

joinWithVect : String -> Vect n String -> String
joinWithVect s = joinWithList s . vectToList

noCharToEmptyStr : Maybe Char -> String
noCharToEmptyStr Nothing = ""
noCharToEmptyStr (Just c) = singleton c

noStrToEmptyStr : Maybe String -> String
noStrToEmptyStr Nothing = ""
noStrToEmptyStr (Just s) = s

charTupleToStr : (Char, Maybe Char) -> String
charTupleToStr (a,b) = strCons a $ noCharToEmptyStr b

-- create a String by replicating a Char
replicate_s : Nat -> Char -> String
replicate_s n c = pack $ List.replicate n c

invert : List (a,b) -> List (b,a)
invert l = (\(a,b) => (b,a)) <$> l
