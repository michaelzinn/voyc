module Main

import Compile.Analyse
import Parse.AllCommands
import Compile.CommodoreBasicCompiler
import Utils.FileIO
import Utils.Utils

indexLine : Line -> (Nat, String)
indexLine l@(MkLine n ss) = (n, show l)

indexProgram : Program -> List (Nat, String)
indexProgram (MkProgram ls) = indexLine <$> ls

resultToReport : Show a => Either String (String, a) -> String
resultToReport (Left s) = "?SYNTAX ERROR\n" ++ s
resultToReport (Right (r, program)) = "PROGRAM PARSED\n" ++ (show program) ++ "\n\nREST\n\"" ++ r ++ "\""

parseLineByLine : List String -> IO ()
parseLineByLine [] = putStrLn "DONE!"
parseLineByLine (l :: ls) = do
  putStr $ trim l ++ "  \t>>>  "
  let Right (r, pl) = parse line l | Left err => (putStrLn $ "?ERROR " ++ err)
  putStrLn $ show pl
  parseLineByLine ls

parseFile : IO ()
parseFile = do
  [binname, inputfilename] <- getArgs | _ => putStrLn "Pass one filename please!"
  putStrLn inputfilename
  putStrLn "ATTEMPTING TO READ FILE"
  Right testSrc <- slurpLines inputfilename | Left err => putStrLn (show err)
  putStrLn "FILE READ SUCCESSFULLY"
  -- putStrLn $ "INPUT SRC\n" ++ joinWith "\n" test1src
  putStrLn "\nATTEMPTING TO PARSE"
  parseLineByLine testSrc
  putStrLn "DONE PARSING!"

rightPad : Nat -> Char -> String -> String
rightPad n c s = s ++ replicate_s (n `minus` length s) c


-- TODO move somewhere else
analyseFile : IO ()
analyseFile = do
  putStrLn "READING FILE"
  [binname, inputfilename] <- getArgs | _ => putStrLn "Pass one filename please!"
  putStrLn inputfilename
  putStrLn "ATTEMPTING TO READ FILE"
  Right testSrc <- slurpLines inputfilename | Left err => putStrLn (show err)
  putStrLn "FILE READ SUCCESSFULLY"
  -- putStrLn $ "INPUT SRC\n" ++ joinWith "\n" test1src
  putStrLn "\nATTEMPTING TO PARSE"
  let p = MkProgram $ (snd <$>) . rights $ parse line <$> testSrc 
  putStrLn "DONE PARSING!"
  putStrLn $ show p

  let progmap = indexProgram p
  let ids = sort $ nub $ (the (List Id)) $ select p
  putStrLn $ "VARS: " ++ (joinWithList ", " $ show <$> ids)
  let jmpTargets : List JmpTarget = sort $ nub $ select p
  -- let ctx = extractContext p
  putStrLn $ "JMPTARGETS: " ++ (joinWithList ", " $ show <$> jmpTargets)
  -- putStrLn $ "Compiled expr " ++ (joinWithList ", " $ show <$> compile (parseExpression "1+1" ctx))
  let sn = constructTextMap p
  let strings : String = joinWithList "\n" $ (\(t,i) => (show i) ++ ":\t" ++ t)  <$> sn
  putStrLn $ "Strings: " ++ strings

  let ctx : CbCtx = extractContext sn p (prepareProgram p)
  putStrLn $ show ctx
  let build = buildPreparedProgram sn p
  -- from calc line to basic line
  let reverseLineMap : List (Nat, Nat) = invert $ reverse $ dictLineLine ctx
  -- basic line number -> basic line
  let indexedLines : List (Nat, String) = reverse $ indexProgram p
  -- calc line number -> calc op
  let indexedOps : List (Nat, String) = zip (enumFromTo 1 (length build)) (show <$> build)
  -- for neat formatting
  let maxBasicLineLength : Nat = foldl max 0 (Strings.length <$> snd <$> indexedLines)
  -- neatly formatted lines
  let indexedLines2 : List (Nat, String) = (\(n,l) => (n , rightPad (maxBasicLineLength + 2) '.' l)) <$> indexedLines
  -- output
  let annotatedOps : List String = (\(on, op)
    => (fromMaybe (replicate_s (maxBasicLineLength + 2) ' ') ((lookup on reverseLineMap) >>= (flip lookup indexedLines2)))
    ++ (show on) ++ " " ++ op
  ) <$> indexedOps

  -- let annotatedOps : List (String, String) = (\(i : Nat , o : CalcOp) => (fromMaybe (replicate_s maxBasicLineLength ' ') (lookup i indexedLines)) ++ "  " ++ o) <$> indexedOps
  let artifact = joinWithList "\n" $ show <$> build
  putStrLn artifact

  putStrLn "OUTPUT........"
  putStrLn $ joinWithList "\n" annotatedOps
  putStrLn "== EOL =="


main : IO ()
main = do 
  parseFile
  analyseFile
