module Parse.AllCommands

-- TODO maybe split this into one command per file? Or will this cause havoc with the "mutual" block?

import public ParserCombinators.Parsers
import ParserCombinators.Urbit
import Data.Vect
import Utils.Utils

%access public export
%default total

-- parser with optional spaces around the word
keyword : String -> Parser String
keyword kw = middle space0 (literal kw) space0

separatedList : String -> Parser a -> Parser (List a)
separatedList s pa = (uncurry (::) <$> pair pa (assert_total (zeroOrMore (right (keyword s) pa))))

----------------------------------------------- Expressions

mutual

  -- Not actually part of the BNF
  data Program = MkProgram (List Line)
  implementation Show Program where
    show (MkProgram ls) = joinWithList "\n" $ showLine <$> ls
  program : Parser Program
  program = MkProgram <$> (assert_total $ zeroOrMore line)

  data Id = MkId String -- {Letter}{Alphanumeric}?[$%]? max 2 chars or 3 like a1$ ?
  showId : Id -> String
  showId (MkId s) = s
  id : Parser Id
  id = (MkId . charTupleToStr) <$> (pair
                                       (matchIs isAlpha)
                                       (maybe (matchIs isAlphaNum))) -- TODO can end with $
                                       -- (left (maybe (matchIs isAlphaNum)) (maybe buc))) -- TODO don't discard the $

  {- TODO later
  data FunctionId     = MkFunctionId String -- FN{Letter}{Letter}?
  showFunctionId : FunctionId -> String
  showFunctionId (MkFunctionId s) = "FN " ++ s
  implementation Show FunctionId where show = showFunctionId
  functionId : Parser FunctionId
  functionId = MkFunctionId <$> charTupleToStr <$> (right
                                                     (literal "FN ")
                                                     (pair 
                                                         (matchIs isAlpha)
                                                         (maybe (matchIs isAlphaNum))))
                                                         -}

  data Line = MkLine Nat Statements
  showLine : Line -> String
  showLine (MkLine n ss) = (show n) ++ " " ++ showStatements ss 
  line : Parser Line
  line = uncurry MkLine <$> pair (left nat space0) (left statements (literal "\n"))-- (oneOrMore statement)

  -- TODO are "Statements" needed or could that be just (List Statement) in Line ?
  data Statements = MkStatements (List Statement) -- TODO nonempty list
  showStatements : Statements -> String
  showStatements (MkStatements ss) = joinWithList ": " $ showStatement <$> ss
  statements : Parser Statements
  statements = MkStatements <$> uncurry (::) <$> pair statement (assert_total (zeroOrMore (right (keyword ":") statement)))

  data Statement = CLOSE Nat
                 | CLR
                 -- | CMD Expression
                 -- | CONT                                          -- Continue
                 -- | DATA ConstantList -- how to use this on the calculator?
                 -- | DEF FunctionId (Vect (S k) Id) Expression   -- !The Id must start with FN
                 | DIM Id (List Expression)
                 | END          
                 | FOR Id Expression Expression StepOpt -- FOR Id=Expression TO Expression [STEP Expression]
                 -- | GET (Maybe Integer) Id
                 | GOSUB Nat -- Original: GOSUB Expression (Can't compile expression)
                 | GOTO Nat -- Original: GOTO Expression (Can't compile expression)
                 | IF Expression ThenClause
                 | INPUT IdList
                 | INPUThax Nat IdList 
                 | LETtis Id Expression  -- also for the LET-free assignment
                 -- | LIST <Line Range>  -- impossible for compiled code
                 -- | LOAD (Vect (S k) Value)
                 -- | NEW
                 | NEXT IdList
                 | ONIdGOTO  Id (List Nat) -- only jump to compiled line numbers.
                 | ONIdGOSUB Id (List Nat) -- only jump to compiled line numbers.
                 -- | OPEN (Vect (S k) Expression)
                 -- | POKE Expression Expression
                 | PRINT PrintList
                 | PRINThax Nat PrintList
                 -- | READ <Id List>
                 | RETURN
                 -- | RESTORE
                 -- | RUN
                 -- | RUN <Expression>
                 -- | STOP
                 -- | SYS Expression -- maybe for inline RPN as in button codes like SYS 42,15
                 -- | WAIT (Vect (S k) Expression)
                 -- | VERIFY (Vect (S k) Expression)
                 | REM String
                 | STOP
  showStatement : Statement      -> String
  showStatement (CLOSE fn)        = "CLOSE " ++ (show fn)
  showStatement  CLR              = "CLR"
  showStatement (DIM i es)        = "DIM " ++ showId i ++ "(" ++ (joinWithList ", " $ showExpression <$> es) ++ ")"
  showStatement  END              = "END"
  showStatement (FOR  i e1 e2 so) = "FOR " ++ showId i ++ "=" ++ showExpression e1 ++ " TO " ++ showExpression e2 ++ showStepOpt so
  showStatement (GOSUB     n    ) = "GOSUB " ++ (show n)
  showStatement (GOTO      n    ) = "GOTO " ++ (show n)
  showStatement (IF        e   t) = "IF " ++ (showExpression e) ++ " THEN " ++ (showThenClause t)
  showStatement (INPUT     il   ) = "INPUT " ++ showIdList il
  showStatement (INPUThax  n  il) = "INPUT#" ++ show n ++ ", " ++  showIdList il
  showStatement (LETtis    id e ) = showId id ++ "=" ++ (showExpression e)
  showStatement (NEXT      il   ) = "NEXT " ++ showIdList il
  showStatement (ONIdGOTO  id ns) = "ON " ++ showId id ++ " GOTO " ++ (joinWithList ", " $ show <$> ns)
  showStatement (ONIdGOSUB id ns) = "ON " ++ showId id ++ " GOSUB " ++ (joinWithList ", " $ show <$> ns)
  showStatement (PRINT     pl   ) = "PRINT" ++ showPrintList pl
  showStatement (PRINThax  n  pl) = "PRINT #" ++ show n ++ "," ++ showPrintList pl
  showStatement  RETURN           = "RETURN"
  showStatement (REM       s    ) = "REM " ++ s
  showStatement  STOP             = "STOP"

  -- make statement parser with n parameter(s)
  mkSP_0 : Statement -> String -> (Parser Statement)
  mkSP_0 st kw = const st <$> keyword kw
  mkSP_1 : (a -> Statement) -> String -> (Parser a) -> (Parser Statement)
  mkSP_1 sc kw pa = sc <$> right (keyword kw) pa
  statement : Parser Statement
  statement = eithers [ mkSP_1   CLOSE  "CLOSE"  nat
                      , uncurry  DIM       <$> (pair (right (keyword "DIM") id) (middle pal (separatedList "," expression) par))
                      , mkSP_0   END    "END"
                      , uncurry4 FOR       <$> (pair 
                                                 (right (keyword "FOR") id)
                                                 (pair
                                                   (right tis expression)
                                                   (pair
                                                     (right (keyword "TO") expression)
                                                     stepOpt)))
                      , mkSP_1   GOSUB  "GOSUB"  nat
                      , mkSP_1   GOTO   "GOTO"   nat
                      , uncurry  IF        <$> (pair (middle (keyword "IF") expression (keyword "THEN") ) thenClause)
                      , mkSP_1   INPUT  "INPUT"  idList
                      , uncurry  LETtis    <$> pair id (right tis expression)
                      , mkSP_1   NEXT   "NEXT"   idList
                      , uncurry  ONIdGOSUB <$> (pair (middle (keyword "ON") id (keyword "GOSUB")) (separatedList "," nat))
                      , uncurry  ONIdGOTO  <$> (pair (middle (keyword "ON") id (keyword "GOTO" )) (separatedList "," nat))
                      , mkSP_1   PRINT  "PRINT"  printList
                      , uncurry  PRINThax  <$> (pair (middle (keyword "PRINT#") nat com) printList)
                      , mkSP_0   RETURN "RETURN"
                      -- , const RUN <$> literal "RUN"
                      , mkSP_1   REM    "REM"    anyStringNoLF
                      , mkSP_0   STOP   "STOP"
                      ]

  data StepOpt = MkStepOptSTEP Expression
               | MkStepOpt
  showStepOpt : StepOpt -> String
  showStepOpt (MkStepOptSTEP e) = "STEP " ++ showExpression e
  showStepOpt  MkStepOpt        = ""
  stepOpt : Parser StepOpt
  stepOpt = either (  MkStepOptSTEP <$> right (keyword "STEP") expression)
                   (const MkStepOpt <$> (literal ""))

  data IdList = MkIdList (List Id) -- can be empty
  showIdList : IdList -> String
  showIdList (MkIdList il) = joinWithList "," $ showId <$> il
  idList : Parser IdList
  idList = MkIdList <$> either
    (separatedList "," id) -- (uncurry (::) <$> pair id (assert_total (zeroOrMore (right (keyword ",") id))))
    (const [] <$> literal "")

{-
  data ValueList = MkValueListValueValueList Value ValueList
                 | MkValueListValue          Value
  showValueList : ValueList -> String
  showValueList (MkValueListValueValueList v vs) = showValue v ++ "," ++ showValueList vs
  showValueList (MkValueListValue          v   ) = showValue v
  valueList : Parser ValueList
  valueList = either (uncurry MkValueListValueValueList <$> pair (left value com) (assert_total valueList))
                     (        MkValueListValue          <$>            value               )
                     -}

  -- TODO replace with native List Constant
  data ConstantList = MkConstantListConstantConstantList Constant ConstantList
                    | MkConstantListConstant             Constant
  showConstantList : ConstantList -> String
  showConstantList (MkConstantListConstantConstantList c cl) = showConstant c ++ "," ++ showConstantList cl
  showConstantList (MkConstantListConstant c) = showConstant c
  constantList : Parser ConstantList
  constantList = either (uncurry MkConstantListConstantConstantList <$> pair (left constant com) (assert_total constantList))
                        (        MkConstantListConstant             <$> constant                             )
  

  data ExpressionList = MkExpressionList (Vect (S k) Expression)
  showExpressionList : ExpressionList -> String
  showExpressionList (MkExpressionList [e]) = showExpression e
  showExpressionList (MkExpressionList (e :: es)) = showExpression e ++ (joinWithVect "" (("," ++) <$> showExpression <$> es))

  data PrintList = MkPrintList (List (Maybe Expression)) -- correct, list may be empty ; 
  showPrintList : PrintList -> String
  showPrintList (MkPrintList es) = (\s => if s /= "" then (" " ++ s) else "")
                                 $ joinWithList ";" 
                                 $ fromMaybe "" <$> (showExpression <$>) <$> es
  printList : Parser PrintList
  printList = MkPrintList <$> either
                (uncurry (::) <$> pair (maybe expression) (assert_total (zeroOrMore (right (keyword ";") (maybe expression)))))
                (const [] <$> literal "")

  {-
  simplifyPrintList : PrintList -> List (Either Constant Id) -- either string or id
  simplifyPrintList (MkPrintList
                     (MkAndExp
                      (MkNotExp
                       (MkCompareExp
                        (MkAddExp
                         (MkMultExp )
                         []
                        )
                       ) :: []
                      ) :: []
                     )
                    ) = 
                      -}

  -- LineRange (only for LIST?)

  data ThenClause = MkThenClauseNat Nat -- GOTO line
                  | MkThenClauseStatement Statement
  showThenClause : ThenClause -> String
  showThenClause (MkThenClauseNat n) = show n
  showThenClause (MkThenClauseStatement s) = assert_total $ showStatement s
  thenClause : Parser ThenClause
  thenClause = either
    (MkThenClauseNat       <$> nat      )
    (MkThenClauseStatement <$> (assert_total statement))



  ---------------------------- Expressions

  data Expression = --MkExpressionOR AndExp Expression  
                   MkExpression (List AndExp) -- TODO should be (Vect (S k) AndExp)
  showExpression : Expression -> String
  showExpression (MkExpression andes) = assert_total $ joinWithList " OR " $ showAndExp <$> andes
  -- showExpression (MkExpressionOR ande e) = assert_total $ showAndExp ande ++ " OR " ++ showExpression e
  -- showExpression (MkExpression   ande  ) = assert_total $ showAndExp ande 
  expression : Parser Expression
  expression = assert_total $ MkExpression <$> uncurry (::) <$> (pair andExp (zeroOrMore (right (keyword "OR") andExp)))

  data AndExp = MkAndExp (List NotExp) -- TODO nonempty list!
  showAndExp : AndExp -> String
  showAndExp (MkAndExp notes) = joinWithList " AND " $ showNotExp <$> notes
  andExp : Parser AndExp
  andExp = MkAndExp <$> uncurry (::) <$> (pair notExp (assert_total (zeroOrMore (right (keyword "AND") notExp))))

  data NotExp = MkNotExpNOT CompareExp 
              | MkNotExp    CompareExp 
  showNotExp : NotExp -> String
  showNotExp (MkNotExpNOT ce) = "NOT " ++ showCompareExp ce
  showNotExp (MkNotExp    ce) =           showCompareExp ce
  notExp : Parser NotExp
  notExp = eithers [ MkNotExpNOT <$> right (keyword "NOT") compareExp
                   , MkNotExp    <$>                       compareExp
                   ]

  data CompareExp = MkCompareExpTis    AddExp CompareExp -- =
                  | MkCompareExpGalGar AddExp CompareExp -- <>
                  | MkCompareExpGar    AddExp CompareExp -- >
                  | MkCompareExpGarTis AddExp CompareExp -- >=
                  | MkCompareExpGal    AddExp CompareExp -- <
                  | MkCompareExpGalTis AddExp CompareExp -- <=
                  | MkCompareExp       AddExp            -- direct boolean, -1 = true?
  showCompareExp : CompareExp -> String
  showCompareExp (MkCompareExpTis    ae ce) = showAddExp ae ++ "="  ++ showCompareExp ce
  showCompareExp (MkCompareExpGalGar ae ce) = showAddExp ae ++ "<>" ++ showCompareExp ce
  showCompareExp (MkCompareExpGar    ae ce) = showAddExp ae ++ ">"  ++ showCompareExp ce
  showCompareExp (MkCompareExpGarTis ae ce) = showAddExp ae ++ ">=" ++ showCompareExp ce
  showCompareExp (MkCompareExpGal    ae ce) = showAddExp ae ++ "<"  ++ showCompareExp ce
  showCompareExp (MkCompareExpGalTis ae ce) = showAddExp ae ++ "<=" ++ showCompareExp ce
  showCompareExp (MkCompareExp       ae   ) = showAddExp ae 
  -- TODO should parse faster, e.g. pair addExp (zeroOrMore (pair operator addExp)) ?
  compareExp : Parser CompareExp
  compareExp = eithers [ mkCE MkCompareExpTis    tis
                       , mkCE MkCompareExpGalGar galgar
                       , mkCE MkCompareExpGar    gar
                       , mkCE MkCompareExpGarTis gartis
                       , mkCE MkCompareExpGal    gal
                       , mkCE MkCompareExpGalTis galtis
                       ,      MkCompareExp  <$>  addExp
                       ]

  -- Added for performance, not part of the BNF directly.
  data AddOperator = MkAddOperatorLus | MkAddOperatorHep
  showAddOperator : AddOperator -> String
  showAddOperator MkAddOperatorLus = "+"
  showAddOperator MkAddOperatorHep = "-"
  addOperator : Parser AddOperator
  addOperator = either (const MkAddOperatorLus <$> (keyword "+"))
                       (const MkAddOperatorHep <$> (keyword "-"))

  data AddExp = MkAddExp MultExp (List (AddOperator, MultExp))
  showAddExp : AddExp -> String
  showAddExp (MkAddExp me aomes) = showMultExp me ++ joinWithList "" ((\(ao,me) => (showAddOperator ao) ++ (showMultExp me)) <$> aomes)
  addExp : Parser AddExp
  addExp = uncurry MkAddExp <$> pair multExp (assert_total (zeroOrMore (pair addOperator multExp)))

  -- this should be higher up, but Idris fails at hoisting.
  mkCE : (AddExp -> CompareExp -> CompareExp) -> Parser String -> Parser CompareExp
  mkCE ces ps = uncurry ces <$> (pair addExp (right ps $ assert_total compareExp))

  -- Added for performance, not part of the BNF directly.
  data MultOperator = MkMultOperatorTar | MkMultOperatorFas
  showMultOperator : MultOperator -> String
  showMultOperator MkMultOperatorTar = "*"
  showMultOperator MkMultOperatorFas = "/"
  multOperator : Parser MultOperator
  multOperator = either (const MkMultOperatorTar <$> keyword "*")
                        (const MkMultOperatorFas <$> keyword "/")

  data MultExp = MkMultExp NegateExp (List (MultOperator, NegateExp))
  showMultExp : MultExp -> String
  showMultExp (MkMultExp ne mones) = (showNegateExp ne)  ++ joinWithList "" ((\(mo,ne) => (showMultOperator mo) ++ (showNegateExp ne)) <$> mones)
  multExp : Parser MultExp
  multExp = uncurry MkMultExp <$> pair negateExp (assert_total (zeroOrMore (pair multOperator negateExp)))

  data NegateExp = MkNegateExpHep PowerExp
                 | MkNegateExp    PowerExp 
  showNegateExp : NegateExp -> String
  showNegateExp (MkNegateExpHep powe) = "-" ++ showPowerExp powe
  showNegateExp (MkNegateExp    powe) =        showPowerExp powe
  negateExp : Parser NegateExp
  negateExp = eithers [ MkNegateExpHep <$> right hep powerExp
                      , MkNegateExp    <$>           powerExp
                      ]

  --                            PowerExp for a^b^c
  data PowerExp = MkPowerExp (List SubExp) -- TODO not empty!
  showPowerExp : PowerExp -> String
  showPowerExp (MkPowerExp subes) = joinWithList "^" $ showSubExp <$> subes
  powerExp : Parser PowerExp
  powerExp = MkPowerExp <$> uncurry (::) <$> pair subExp (assert_total (zeroOrMore ((right (keyword "^") subExp))))

  data SubExp = MkSubExpPalPar Expression
              | MkSubExp Value
  showSubExp : SubExp -> String
  showSubExp (MkSubExpPalPar e) = "(" ++ showExpression e ++ ")"
  showSubExp (MkSubExp       v) =        showValue      v
  subExp : Parser SubExp
  subExp = eithers [ MkSubExpPalPar <$> middle pal expression par
                   , MkSubExp       <$>            value
                   ]

  data Value = MkValueId Id
             | ABS Expression
             | ASC Expression
             | ATN Expression
             | CHRbuc Expression
             | COS Expression
             | EXP Expression
             -- | MkValueFN FunctionId ExpressionList -- (Vect (S k) Expression) -- TODO later
             | FRE Value --                 !The <Value> is  irrelevant
             | INT Expression
             | LEFTbuc Expression Expression
             | LEN Expression
             | PEEK Expression
             | POS Value --        '(' <Value> ')'                  !The <Value> is  irrelevant
             | RIGHTbuc Expression Expression
             | RND Expression
             | SGN Expression
             | SIN Expression
             | SPC Expression
             | SQR Expression
             | TAB Expression
             | TAN Expression
             | VAL Expression
             | MkValueConstant Constant 
  mkShowValue : String -> List Expression -> String
  mkShowValue s es = s ++ "(" ++ (joinWithList "," $ showExpression <$> es) ++ ")"
  showValue : Value -> String
  showValue (MkValueId id) = (showId id)
  showValue (ABS      e    ) = mkShowValue "ABS"     [e     ]
  showValue (ASC      e    ) = mkShowValue "ASC"     [e     ]
  showValue (ATN      e    ) = mkShowValue "ATN"     [e     ]
  showValue (CHRbuc   e    ) = mkShowValue "CHR$"    [e     ]
  showValue (COS      e    ) = mkShowValue "COS"     [e     ]
  showValue (EXP      e    ) = mkShowValue "EXP"     [e     ]
  --  showValue (MkValueFN fid el) = "FN " ++ showFunctionId fid ++ "(" ++ showExpressionList el ++ ")"
  showValue (FRE      v    ) = "FRE(" ++ showValue v ++ ")"
  showValue (INT      e    ) = mkShowValue "INT"     [e     ]
  showValue (LEFTbuc  le re) = mkShowValue "LEFT$("  [le, re]
  showValue (LEN      e    ) = mkShowValue "LEN"     [e     ]
  showValue (PEEK     e    ) = mkShowValue "PEEK"    [e     ]
  showValue (POS      v    ) = "POS(" ++ showValue v ++ ")" -- v doesn't matter
  showValue (RIGHTbuc le re) = mkShowValue "RIGHT$(" [le, re]
  showValue (RND      e    ) = mkShowValue "RND"     [e     ]
  showValue (SGN      e    ) = mkShowValue "SGN"     [e     ]
  showValue (SIN      e    ) = mkShowValue "SIN"     [e     ]
  showValue (SPC      e    ) = mkShowValue "SPC"     [e     ]
  showValue (SQR      e    ) = mkShowValue "SQR"     [e     ]
  showValue (TAB      e    ) = mkShowValue "TAB"     [e     ]
  showValue (TAN      e    ) = mkShowValue "TAN"     [e     ]
  showValue (VAL      e    ) = mkShowValue "VAL"     [e     ]
  showValue (MkValueConstant c) = showConstant c

  mkValueParser : (Expression -> Value) -> String -> Parser Value
  mkValueParser vconst s = vconst <$> right (keyword s) (middle (keyword "(") expression (keyword ")"))

  value : Parser Value
  value = eithers $ (uncurry mkValueParser <$>
                    [ (ABS   , "ABS" )
                    , (ATN   , "ATN" )
                    , (CHRbuc, "CHR$")
                    , (COS   , "COS" )
                    , (EXP   , "EXP" )
                    , (INT   , "INT" )
                    , (PEEK  , "PEEK")
                    , (RND   , "RND" )
                    , (SGN   , "SGN" )
                    , (SIN   , "SIN" )
                    , (SQR   , "SQR" )
                    , (TAB   , "TAB" )
                    , (TAN   , "TAN" )
                    ]) ++
                    [ MkValueConstant <$> constant
                    , MkValueId       <$> id
                    ]

  data Constant = MkConstantInteger Nat -- sign is wrapped via NegExp
                | MkConstantString String 
                | MkConstantReal String -- find a decimal type?
  showConstant : Constant -> String
  showConstant (MkConstantInteger n) = (show n)
  showConstant (MkConstantString s) = "\"" ++ s ++ "\""
  showConstant (MkConstantReal r) = r
  constant : Parser Constant
  constant = eithers [ MkConstantReal <$> (\(l,r) => pack l ++ "." ++ pack r) <$> (pair (left (assert_total $ zeroOrMore digit) dot) (assert_total $ oneOrMore digit))
                     , MkConstantInteger <$> nat
                     ,  MkConstantString <$> quotedString
                     ]

-- The Shows must be outside of mutual, but the shows must be inside O_o
implementation Show Id             where show = showId
-- implementation Show Lines          where show = showLines
implementation Show Line           where show = showLine
implementation Show Statements     where show = showStatements
implementation Show Statement      where show = showStatement
implementation Show StepOpt        where show = showStepOpt
implementation Show IdList         where show = showIdList
-- implementation Show ValueList      where show = showValueList
implementation Show ConstantList   where show = showConstantList
implementation Show ExpressionList where show = showExpressionList
implementation Show PrintList      where show = showPrintList
-- (LineRange)
implementation Show ThenClause     where show = showThenClause
-- Expressions
implementation Show Expression     where show = showExpression
implementation Show AndExp         where show = showAndExp
implementation Show NotExp         where show = showNotExp
implementation Show CompareExp     where show = showCompareExp
implementation Show AddExp         where show = showAddExp
implementation Show MultExp        where show = showMultExp
implementation Show NegateExp      where show = showNegateExp                                         
implementation Show PowerExp       where show = showPowerExp
implementation Show SubExp         where show = showSubExp
implementation Show Value          where show = showValue
implementation Show Constant       where show = showConstant

implementation Eq Id where
  (==) (MkId s1) (MkId s2) = s1 == s2
  (/=) (MkId s1) (MkId s2) = s1 /= s2

implementation Ord Id where
  compare (MkId s1) (MkId s2) = compare s1 s2 

