module Test.Tests


import Compile.Analyse
import Parse.AllCommands
import Compile.CommodoreBasicCompiler
import Utils.FileIO
import Utils.Utils

%access export

indexLine : Line -> (Nat, String)
indexLine l@(MkLine n ss) = (n, show l)

indexProgram : Program -> List (Nat, String)
indexProgram (MkProgram ls) = indexLine <$> ls

h1 : String -> IO ()
h1 s = putStrLn $ "\n=== " ++ s ++ " ==="

test : Show a => Parser a -> String -> IO ()
test p s = do
  putStrLn $ "<- " ++ s
  let Right (s, ls) = parse p s | Left err => (putStrLn $ "?FAILED TO PARSE:" ++ err)
  putStrLn $ "  -> " ++ (show ls)

runTests : IO ()
runTests = do
  h1 "NEXT"
  test statement "NEXT"
  test statement "NEXT N"
  test statement "NEXT A,B,C"

  h1 "FOR"
  test statement "FOR I=1 TO 10"
  test value "CHR$(7)"
  test statement "PRINT CHR$(7);" 
  test statements "PRINT" --fail
  test statements "PRINT: FOR N=1 TO 10" --fail
  test statements "PRINT: FOR N=1 TO 10: PRINT CHR$(7)" --fail
  test statements "PRINT: FOR N=1 TO 10: PRINT CHR$(7): NEXT N" --fail
  test statements "PRINT: FOR N=1 TO 10: PRINT CHR$(7);: NEXT N" --fail
  test line "990 PRINT: FOR N=1 TO 10: PRINT CHR$(7);: NEXT N\n" --fail

  h1 "REM"
  test statement "REM huh"
  test statement "REM *** TRYING TO USE MORE GRAIN THAN IS IN SILOS?"
  test line "418 REM *** TRYING TO USE MORE GRAIN THAN IS IN SILOS?\n"

  h1 "CONSTANTS"
  test constant "5"
  test constant "\"ach was\""
  test constant "0.5"
  test constant ".4"

  h1 "HAMMURABI"
  {-
  test line "310 Y=17\n"
  test line "310 Y=C\n"
  test line "310 Y=C+17\n"
  test line "310 C=10\n"
  test line "310 C=RND(1)\n"
  test statements "C=10*RND(1)"
  test line "310 C=10*RND(1)\n"
  test statements "C=INT(10)"
  test value "INT(10*RND(1))"
  test statements "C=INT(1*1)"
  test statements "C=INT(RND(1))"
  -}
  test statements "C=INT(1)" -- slow
  test statements "C=INT(INT(1))" -- fail
  test statements "C=INT(INT(INT(1)))"
  test statements "C=INT(INT(INT(INT(1))))"
  test statements "C=INT(10*RND(1))" -- fail
  test line "310 C=INT(10*RND(1))\n"
  test line "310 C=INT(10*RND(1)): Y=C+17\n"
  test expression "P/2"
  test expression "INT(1)"
  test expression "INT(P/2)"
  test statement  "P=1"
  test statement  "P=INT(P/2)"
  test statements  "P=INT(P/2):END"
  test statements  "P=INT(P/2)"
  test line "228 P=INT(P/2):END\n"
  test line "228 P=INT(P/2)\n"

  h1 "QUOTED STRING"
  test quotedString "\"hey\""

  h1 "CONSTANT"
  test constant "\"hey\""
  test constant "123"

  h1 "VALUE"
  test value "\"hey\""
  test value "123"

  h1 "SUBEXP"
  test subExp "\"hey\""

  h1 "POWEREXP"
  test powerExp "\"hey\""

  h1 "NEGATEEXP"
  test negateExp "\"hey\""

  h1 "EXPRESSION"
  test expression "\"hey\""
  test expression "3"
  test expression "3+4"
  test expression "PRINT"
  --test expression "(1)"
  --test expression "(2+3)"
  --test expression "2 * ( 3 + 4 )"

  h1 "PRINTLIST"
  test printList "\"hey\";\"hey\""
  test printList "\"hey\""
  test printList ""

  h1 "STATEMENT"
  test statement "PRINT"

  h1 "STATEMENTLIST"
  test statements "A=1: B=2"
  test statements "A=1:B=2"
  test statements "A=1:PRINT"
  test statements "PRINT: B=2"
  test statements "PRINT:PRINT:PRINT"

  h1 "ASSIGNMENTS"
  test expression "9"
  test multExp "3*4"
  test multExp "A/4"
  test statement "A=2"
  test statement "A=2*2"
  test line "10 A=2*2\n"
  test value "INT(1)"
  test statements "A=RND(1)"
  test line "20 B=INT(C)\n" -- fails :(

  h1 "LINES"
  test line "10 END\n"
  test line "0 A=9\n"
  test line "999 PRINT A\n"
  test line "20 GOTO 10\n"

--

testCode : String
testCode = "95 D1=0: P1=0\n"
        ++ "95 D1=0: P1=0\n"
        ++ "100 Z=0:P=95:S=2800: H=3000: E=H-S\n"
        ++ "100 Z=0\n"--: P=95:S=2800: H=3000: E=H-S\n"


testCompile : IO ()
testCompile = do
  let Right (r, exp) = parse expression "((Z-1)*P1+D*100/P)/Z" | Left err => (putStrLn $ "?ERROR " ++ show err)
  let ctx = MkCbCtx [(MkId "Z",0),(MkId "P1",1),(MkId "D",2),(MkId "P",3),(MkId "Z",4)] []
  let pomcile = compileExpression [] exp 
  putStrLn $ "Nah!" -- formatCompiledCode pomcile


{-
main : IO ()
main = do 
  runTests
  -}
