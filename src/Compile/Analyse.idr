module Compile.Analyse

%access public export
%default total

||| Selects a list of ys that are somewhere inside x
interface Select y x where
  select : x -> List y


