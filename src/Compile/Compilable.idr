module Compile.Compilable

%access public export
%default total

{-
  The context is meant to contain things like var->register mapping or goto num->label
-}

interface Compilable code context target where
  compile : code -> context -> target
