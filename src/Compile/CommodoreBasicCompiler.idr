module Compile.CommodoreBasic

import Compile.Analyse
import Compile.Compilable
import Parse.AllCommands
import Data.Vect
import Utils.Utils

%access public export
%default total

{-
Problem:
The NEXT statement does not necessary jump to a line number, it jumps to the statement after the corresponding FOR:
10 PRINT:FOR I=1 TO 10:PRINT "JUMP HERE":NEXT I

Solution:
Instead of having numbered lines containing multiple statements, have statements of which some are labeled with zero or more line numbers.
This is also important when "compiling" comments, which just disappear:

10 REM COMMENT 1
20 REM COMMENT 2
30 PRINT "LABEL 10, 20, 30": PRINT "NO LABEL"
40 PRINT "LABEL 40"
50 REM "GOTO 50 WOULD BE PROBLEMATIC, SO INSERT AN END"

becomes:
[([10,20,30], <PRINT "LABEL 10, 20, 30">)
,([        ], <PRINT "NO LABEL">)
,([40      ], <PRINT "LABEL 40">)
,([50      ], <END>)
]
-}


{-
Alternative representation of a BASIC Program.
Instead of having numbered lines containing statements,
this has a list of statements of which some are annotated with line numbers.
Comments are removed, comments' line numbers flow to the next non-comment statement.
If the program ends with a comment, the line numbers disappear, which makes it 
impossible to jump to the last line of a program if it is a comment. But nobody does this in practice.
-}
data PreparedProgram = MkPreparedProgram (List (List Nat, Statement))
implementation Show PreparedProgram where
  show (MkPreparedProgram ass) =  "TODO dammit"
  {-
    joinWithList "" $
    (\(lineNumbers , statement) =>
      if (empty lineNumbers)
         then (":" ++ showStatement statement)
         else (joinWithList "" (("\n" ++) <$> (show <$> lineNumbers)) ++ showStatement statement)) <$>
    ass
    -}




lineToAnnotatedStatements : Line -> List (List Nat, Statement)
lineToAnnotatedStatements (MkLine n (MkStatements (fs :: rss))) = ([n], fs) :: ((MkPair []) <$> rss)
lineToAnnotatedStatements (MkLine n (MkStatements [])) = [([n], REM "?This can't happen, because the statement list of a line is never empty.")]

-- allow jumping to "comments" by making the comment's line number flow to the next statement.
prepareProgram : Program -> PreparedProgram
prepareProgram (MkProgram lines) = 
  let
    withComments    : List (List Nat, Statement) =
      lines >>= lineToAnnotatedStatements
    withoutComments : List (List Nat, Statement) =
      fst $ (foldl (\
        (annotatedStatements, overflowingLineNumbers),
        (statementLineNumbers, statement) 
        => case statement of
                (REM s) => (annotatedStatements, 
                            overflowingLineNumbers ++ statementLineNumbers) -- remove, accumulate overflow
                other   => (annotatedStatements ++ [(overflowingLineNumbers ++ statementLineNumbers, statement)],
                            []))
        (the (List (List Nat, Statement), List Nat) ([], [])) -- no annotatedStatements, no overflowingLineNumbers
        withComments)
  in
    MkPreparedProgram withoutComments



-- Searching (List ID) in the whole program.

implementation Cast (Vect k a) (List a) where
  cast [] = []
  cast (x :: xs) = x :: cast xs

-- target line for GOTO, GOSUB, IFTHEN n, maybe FOR/NEXT
data JmpTarget = MkJmpTarget Nat
implementation Show JmpTarget where
    show (MkJmpTarget jt) = show jt
implementation Eq JmpTarget where
  (==) (MkJmpTarget jt1) (MkJmpTarget jt2) = jt1 == jt2
  (/=) (MkJmpTarget jt1) (MkJmpTarget jt2) = jt1 == jt2
implementation Ord JmpTarget where
  compare (MkJmpTarget jt1) (MkJmpTarget jt2) = compare jt1 jt2


mutual 

  selectIdProgram : Program -> List Id
  selectIdProgram (MkProgram ll) = join $ selectIdLine <$> ll
  selectJmpTargetProgram : Program -> List JmpTarget
  selectJmpTargetProgram (MkProgram ll) = join $ selectJmpTargetLine <$> ll
  selectStringProgram : Program -> List String
  selectStringProgram (MkProgram ll) = ll >>= selectStringLine

  selectIdPreparedProgram : PreparedProgram -> List Id
  selectIdPreparedProgram (MkPreparedProgram ass) = ass >>= (selectIdStatement . snd)
  selectJmpTargetPreparedProgram : PreparedProgram -> List JmpTarget
  selectJmpTargetPreparedProgram (MkPreparedProgram ass) = ass >>= (selectJmpTargetStatement . snd)
  selectStringPreparedProgram : PreparedProgram -> List String
  selectStringPreparedProgram (MkPreparedProgram ass) = ass >>= (selectStringStatement . snd)

  selectIdLine : Line -> List Id
  selectIdLine (MkLine n ss) = selectIdStatements ss
  selectJmpTargetLine : Line -> List JmpTarget
  selectJmpTargetLine (MkLine n ss) = selectJmpTargetStatements ss
  selectStringLine : Line -> List String
  selectStringLine (MkLine n ss) = selectStringStatements ss

  selectIdStatements : Statements -> List Id
  selectIdStatements (MkStatements ss) = join $ selectIdStatement <$> ss
  selectJmpTargetStatements : Statements -> List JmpTarget
  selectJmpTargetStatements (MkStatements ss) = join $ selectJmpTargetStatement <$> ss
  selectStringStatements : Statements -> List String
  selectStringStatements (MkStatements ss) = ss >>= selectStringStatement

  selectIdStatement : Statement -> List Id
  selectIdStatement (FOR i e1 e2 so) = i :: ( selectIdExpression e1 ++ selectIdExpression e2 ++ selectIdStepOpt so )
  selectIdStatement (IF e t) = selectIdExpression e ++ selectIdThenClause t
  selectIdStatement (INPUT il) = selectIdIdList il
  selectIdStatement (INPUThax n il) = selectIdIdList il
  selectIdStatement (LETtis i e) = i :: selectIdExpression e
  selectIdStatement (NEXT il) = selectIdIdList il
  selectIdStatement (PRINT pl) = selectIdPrintList pl
  selectIdStatement (PRINThax n pl) = selectIdPrintList pl
  selectIdStatement _ = []
  selectJmpTargetStatement : Statement -> List JmpTarget
  selectJmpTargetStatement (GOSUB n) = [MkJmpTarget n]
  selectJmpTargetStatement (GOTO n) = [MkJmpTarget n]
  selectJmpTargetStatement (IF e tc) = selectJmpTargetThenClause tc
  selectJmpTargetStatement _ = []
  selectStringStatement : Statement -> List String
  selectStringStatement (PRINT pl) = selectStringPrintList pl
  selectStringStatement _ = []

  selectIdStepOpt : StepOpt -> List Id
  selectIdStepOpt (MkStepOptSTEP e) = selectIdExpression e
  selectIdStepOpt MkStepOpt = []

  selectIdIdList : IdList -> List Id
  selectIdIdList (MkIdList ids) = ids

  --selectIdValueList : ValueList -> List Id
  --selectIdValueList (MkValueList l) = join $ selectIdValue <$> l

  selectIdExpressionList : ExpressionList -> List Id
  selectIdExpressionList (MkExpressionList e) = join $ cast $ selectIdExpression <$> e

  selectIdPrintList : PrintList -> List Id
  selectIdPrintList (MkPrintList pl) = join $ selectIdExpression <$> catMaybes pl
  selectStringPrintList : PrintList -> List String
  selectStringPrintList (MkPrintList pl) = catMaybes pl >>= selectStringExpression

  selectIdThenClause : ThenClause -> List Id
  selectIdThenClause (MkThenClauseNat _) = []
  selectIdThenClause (MkThenClauseStatement s) = selectIdStatement s
  selectJmpTargetThenClause : ThenClause -> List JmpTarget
  selectJmpTargetThenClause (MkThenClauseNat n) = [MkJmpTarget n]
  selectJmpTargetThenClause (MkThenClauseStatement s) = selectJmpTargetStatement s
  selectStringThenClause : ThenClause -> List String
  selectStringThenClause (MkThenClauseStatement s) = selectStringStatement s
  selectStringThenClause (MkThenClauseNat       n) = []

  selectIdExpression : Expression -> List Id
  selectIdExpression (MkExpression lae) = join $ selectIdAndExp <$> lae
  selectStringExpression : Expression -> List String
  selectStringExpression (MkExpression lae) = assert_total $ lae >>= selectStringAndExp

  selectIdAndExp : AndExp -> List Id
  selectIdAndExp (MkAndExp lne) = join $ selectIdNotExp <$> lne
  selectStringAndExp : AndExp -> List String
  selectStringAndExp (MkAndExp lne) = lne >>= selectStringNotExp

  selectIdNotExp : NotExp -> List Id
  selectIdNotExp (MkNotExpNOT ce) = selectIdCompareExp ce
  selectIdNotExp (MkNotExp ce) = selectIdCompareExp ce
  selectStringNotExp : NotExp -> List String
  selectStringNotExp (MkNotExpNOT ce) = []
  selectStringNotExp (MkNotExp ce) = selectStringCompareExp ce

  selectIdCompareExp : CompareExp -> List Id
  selectIdCompareExp _ = [] -- TODO this is a lie, but it's fine, because LETtis should find them all.
  selectStringCompareExp : CompareExp -> List String
  selectStringCompareExp (MkCompareExpTis    ae ce) = selectStringAddExp ae ++ selectStringCompareExp ce
  selectStringCompareExp (MkCompareExpGalGar ae ce) = selectStringAddExp ae ++ selectStringCompareExp ce
  selectStringCompareExp (MkCompareExpGar    ae ce) = selectStringAddExp ae ++ selectStringCompareExp ce
  selectStringCompareExp (MkCompareExpGarTis ae ce) = selectStringAddExp ae ++ selectStringCompareExp ce
  selectStringCompareExp (MkCompareExpGal    ae ce) = selectStringAddExp ae ++ selectStringCompareExp ce
  selectStringCompareExp (MkCompareExpGalTis ae ce) = selectStringAddExp ae ++ selectStringCompareExp ce
  selectStringCompareExp (MkCompareExp       ae   ) = selectStringAddExp ae

  selectIdAddExp : AddExp -> List Id
  selectIdAddExp (MkAddExp me laome) = join $ selectIdMultExp <$> me :: (snd <$> laome)
  selectStringAddExp : AddExp -> List String
  selectStringAddExp (MkAddExp me laome) = (me :: (snd <$> laome)) >>= selectStringMultExp

  selectIdMultExp : MultExp -> List Id
  selectIdMultExp (MkMultExp ne mones) = join $ selectIdNegateExp <$> ne :: (snd <$> mones)
  selectStringMultExp : MultExp -> List String
  selectStringMultExp (MkMultExp ne mones) = (ne :: (snd <$> mones)) >>= selectStringNegateExp

  selectIdNegateExp : NegateExp -> List Id
  selectIdNegateExp (MkNegateExpHep pe) = selectIdPowerExp pe
  selectIdNegateExp (MkNegateExp    pe) = selectIdPowerExp pe
  selectStringNegateExp : NegateExp -> List String
  selectStringNegateExp (MkNegateExpHep pe) = selectStringPowerExp pe
  selectStringNegateExp (MkNegateExp    pe) = selectStringPowerExp pe

  selectIdPowerExp : PowerExp -> List Id
  selectIdPowerExp (MkPowerExp ses) = join $ selectIdSubExp <$> ses
  selectStringPowerExp : PowerExp -> List String
  selectStringPowerExp (MkPowerExp ses) = ses >>= selectStringSubExp

  selectIdSubExp : SubExp -> List Id
  selectIdSubExp (MkSubExpPalPar e) = selectIdExpression e
  selectIdSubExp (MkSubExp v) = selectIdValue v
  selectStringSubExp : SubExp -> List String
  selectStringSubExp (MkSubExpPalPar e) = selectStringExpression e
  selectStringSubExp (MkSubExp v) = selectStringValue v


  selectIdValue : Value -> List Id
  selectIdValue (MkValueId x) = pure x
  selectIdValue (ABS x) = selectIdExpression x
  selectIdValue (ASC x) = selectIdExpression x
  selectIdValue (ATN x) = selectIdExpression x
  selectIdValue (CHRbuc x) = selectIdExpression x
  selectIdValue (COS x) = selectIdExpression x
  selectIdValue (EXP x) = selectIdExpression x
  selectIdValue (FRE x) = selectIdValue x
  selectIdValue (INT x) = selectIdExpression x
  selectIdValue (LEFTbuc x y) = selectIdExpression x ++ selectIdExpression y
  selectIdValue (LEN x) = selectIdExpression x
  selectIdValue (PEEK x) = selectIdExpression x
  selectIdValue (POS x) = selectIdValue x
  selectIdValue (RIGHTbuc x y) = selectIdExpression x ++ selectIdExpression y
  selectIdValue (RND x) = selectIdExpression x
  selectIdValue (SGN x) = selectIdExpression x
  selectIdValue (SIN x) = selectIdExpression x
  selectIdValue (SPC x) = selectIdExpression x
  selectIdValue (SQR x) = selectIdExpression x
  selectIdValue (TAB x) = selectIdExpression x
  selectIdValue (TAN x) = selectIdExpression x
  selectIdValue (VAL x) = selectIdExpression x
  selectIdValue (MkValueConstant x) = []
  selectStringValue : Value -> List String
  selectStringValue (MkValueConstant x) = selectStringConstant x
  selectStringValue _ = []
  
  selectStringConstant : Constant -> List String
  selectStringConstant (MkConstantString s) = [s]
  selectStringConstant _ = []


implementation Select Id Program         where select = selectIdProgram
implementation Select Id PreparedProgram where select = selectIdPreparedProgram
implementation Select Id Line            where select = selectIdLine
implementation Select Id Statements      where select = selectIdStatements
implementation Select Id Statement       where select = selectIdStatement
implementation Select Id StepOpt         where select = selectIdStepOpt
implementation Select Id IdList          where select = selectIdIdList
-- implementation Select Id ValueList where select = selectIdValueList
implementation Select Id ExpressionList  where select = selectIdExpressionList

implementation Select JmpTarget Program  where select = selectJmpTargetProgram

-- ugly hack
surely : Maybe a -> a
surely (Just a) = a
surely Nothing = idris_crash "Not so sure now, huh?"

-- Matrix Identifiers
data MtxId = MtxA | MtxB | MtxC | MtxD | MtxE

implementation Show MtxId where
  show MtxA = "A"
  show MtxB = "B"
  show MtxC = "C"
  show MtxD = "D"
  show MtxE = "E"

-- CommodoreBasicContext
record CbCtx where
  constructor MkCbCtx
  dictIdRegister : List (Id, Nat)
  dictIdMatrix : List (Id, MtxId)
  dictLineLine : List (Nat, Nat) -- BASIC line -> calculator line
  dictForLine : List (Id, Nat) -- FOR loop counter variable line to calculator line
implementation Show CbCtx where
  show (MkCbCtx rs ms ls fs) = 
    "CommodoreBASIC programming context:"
       ++ "\n  Register map: " ++ show rs
       ++ "\n  Matrix map: " ++ show ms
       ++ "\n  Line map: " ++ show ls
       ++ "\n  FOR map: " ++ show fs

maybeLookupRegister : Id -> CbCtx -> Maybe Nat
maybeLookupRegister i ctx = lookup i $ dictIdRegister ctx

lookupRegister : Id -> CbCtx -> Nat
lookupRegister   i     ctx    = fromMaybe (idris_crash "bad context register lookup") $ maybeLookupRegister i ctx

maybeLookupMatrix : Id -> CbCtx -> Maybe MtxId
maybeLookupMatrix i ctx = lookup i $ dictIdMatrix ctx

lookupMatrix : Id -> CbCtx -> MtxId
lookupMatrix   i     ctx    = fromMaybe (idris_crash "bad context matrix lookup") $ maybeLookupMatrix i ctx

lookupLine : Nat -> CbCtx -> Nat
lookupLine n ctx = fromMaybe (idris_crash "bad context line lookup") $ lookup n $ dictLineLine ctx

data CalcMacro = MkCalcMacroBasicLine Nat -- can't be compiled directly because of ENTER insertion
               | MkCalcMacroStoId Id
               | MkCalcMacroRclId Id

opCountCalcMacro : CalcMacro -> Nat
opCountCalcMacro (MkCalcMacroBasicLine n) = 3 -- 3 digits for the number
opCountCalcMacro (MkCalcMacroStoId n) = 1 -- STO .5
opCountCalcMacro (MkCalcMacroRclId n) = 1 -- RCL .5

-- these are arranged exactly like they appear on a HP 15-C calculator
data CalcOp  = -----------------------------------------------------------------------------------------------

   SQRX   |      EKetX              |  YKetX                    |  CHS   | Digit7 | Digit8 | Digit9 |   Fas
          |       LN                                            |  ABS                              | XGalTisY
--------------------------------------------------------------------------------------------------------------
| LBL Int                 |DIM MtxId
          | GTO Nat|GTO_I |  SIN    |   COS        | TAN        |  EEX   | Digit4 | Digit5 | Digit6 |   Tar
                                                   | TANKetHep1          | SF Nat | CF Nat          |  XTis0
--------------------------------------------------------------------------------------------------------------
                                                                | RANHax
|  RFasS  | GSB Nat|GSB_I | R_down  | XGalGarY                  |  ENTER | Digit1 | Digit2 | Digit3 |   Hep
          |       RTN     |  R_up                  |    CLX                                         | TEST Int
-----------------------------------------------------------------        -------------------------------------
                                    |  STOg MtxId  | RCL_PaliPar 
                                    |STO Nat|STO_I |RCL Nat|RCL_I        | Digit0 |  Dot            |   Lus
                                    |INT|STO_RANHax| RCLg MtxId
--------------------------------------------------------------------------------------------------------------
| NotImplemented -- fake op, kinda like a hole. Allows for incomplete compilations. Should be deleted later.


-- this sits at the start of program memory.
runtimeLibrary : List CalcOp
runtimeLibrary = [ LBL 0 -- PRINT
                 ,   RFasS
                 ,   CF 8
                 ,   RTN
                 , LBL 1 -- GTO/GSB friendly because RTN doesn't work if a line number sub contains R/S
                 ,   CHS
                 ,   STO_I
                 ,  LBL 2 -- Use instead of GSB I for line numbers
                 ,   GTO_I
                 , LBL 3 -- ON stuff. Spaghetti code, this can only be understood in the context of ON only.
                 ,   CHS
                 ,   XGalGarY
                 ,   Digit1
                 ,   Hep
                 ,   TEST 0 -- x!=0
                 ,     RTN
                 ,   R_down
                 ,   STO_I
                 ,   R_up
                 ,   RTN
                 , LBL 4 -- RCL (x). This is also PEEK
                 ,   STO_I
                 ,   CLX
                 ,   RCL_PaliPar
                 ,   RTN
                 , LBL 21 -- LBL B, start of program
                 ]

-- helpful mnemonics
mPRINT : CalcOp
mPRINT = GSB 0
mGTOlineX : CalcOp
mGTOlineX = GTO 1
mGSBlineX : CalcOp
mGSBlineX = GSB 1
mGSBlineI : CalcOp
mGSBlineI = GSB 2
mONSpaghetti : CalcOp
mONSpaghetti = GSB 3
mRCL_PalXPar : CalcOp
mRCL_PalXPar = GSB 4

formatNat : Nat -> String
formatNat (S (S (S (S (S (S (S (S (S (S n)))))))))) = "." ++ show n
formatNat n = show n

formatLblNum : Int -> String
formatLblNum 20 = "A"
formatLblNum 21 = "B"
formatLblNum 22 = "C"
formatLblNum 23 = "D"
formatLblNum 24 = "E"
formatLblNum n = formatNat $ cast n

implementation Show CalcOp where
  show SQRX = "sqrt" ; show EKetX = "e^x" ; show YKetX = "y^x" ; show CHS = "CHS" ; show Digit7 = "7" ; show Digit8 = "8" ; show Digit9 = "9" ; show   Fas    =  "/" 
                       show LN    = "LN"  ;                      show ABS = "ABS" ;                                                             show XGalTisY = "x<=y"

  show (LBL n) = "LBL " ++ formatLblNum n; show (DIM m) = "DIM " ++ show m
  show GTO_I = "GTO I"
  show (GTO n) = "GTO " ++ formatNat n ; show SIN = "SIN" ; show COS = "COS" ; show TAN = "TAN" ; show TANKetHep1 = "TAN^-1" ; show EEX = "EEX"
  show Digit4 = "4" ; show Digit5 = "5" ; show Digit6 = "6" ; show Tar = "*"
         show (SF n) = "SF " ++ show n ; show (CF n) = "CF " ++ show n ; show XTis0 = "x=0"

  show GSB_I = "GSB I" ; show RANHax = "RAN#"
  show RFasS = "R/S" ; show (GSB n) = "GSB " ++ formatNat n ; show R_down = "R_down" ; show XGalGarY = "x<>y" ; show ENTER = "ENTER" ;
  show Digit1 = "1" ; show Digit2 = "2" ; show Digit3 = "3" ; show Hep = "-"
  show RTN = "RTN" ; show R_up = "R_up" ; show CLX = "CLx"  ; show (TEST i) = "TEST " ++ show i

  show (STO n) = "STO " ++ formatNat n ; show STO_I = "STO I" ; show (STOg m) = "STO g " ++ show m ; show STO_RANHax = "STO RAN#"
  show (RCL n) = "RCL " ++ formatNat n ; show RCL_I = "RCL I" ; show RCL_PaliPar = "RCL (i)" ; show (RCLg m) = "RCL g " ++ show m

  show Digit0 = "0" ; show Dot = "." ; show Lus = "+"
  show INT = "INT"

  show NotImplemented = "<OpCode Not Implemented>" -- TODO remove eventually.

{-
  show XHax0 = "x#0"
  show XGar0 = "x>0"
  show XGal0 = "x<0"
  show XGarTis0 = "x>=0"
  show XGalTis0 = "x<=0"
  show XTisY = "x=y"
  show XHaxY = "x#y"
  show XGalY = "x<y"
  show XGarTisY = "x>=y"
  show XTis0 = "x=0"
  show XGalTisY= "x<=y"
  show (TEST 0) = "x#0"
  show (TEST 1) = "x>0"
  show (TEST 2) = "x<0"
  show (TEST 3) = "x>=0"
  show (TEST 4) = "x<=0"
  show (TEST 5) = "x=y"
  show (TEST 6) = "x#y"
  show (TEST 7) = "x>y"
  show (TEST 8) = "x<y"
  show (TEST 9) = "x>=y"

  -}

isDigitComponent : CalcOp -> Bool
isDigitComponent Digit0 = True
isDigitComponent Digit1 = True
isDigitComponent Digit2 = True
isDigitComponent Digit3 = True
isDigitComponent Digit4 = True
isDigitComponent Digit5 = True
isDigitComponent Digit6 = True
isDigitComponent Digit7 = True
isDigitComponent Digit8 = True
isDigitComponent Digit9 = True
isDigitComponent Dot = True
isDigitComponent _ = False

startsWithDigitComponent : Either CalcMacro CalcOp -> Bool
startsWithDigitComponent (Left (MkCalcMacroBasicLine n)) = True
startsWithDigitComponent (Left (MkCalcMacroStoId i)) = False
startsWithDigitComponent (Left (MkCalcMacroRclId i)) = False
startsWithDigitComponent (Right n) = isDigitComponent n

endsWithDigitComponent : Either CalcMacro CalcOp -> Bool
endsWithDigitComponent (Left m) = False
endsWithDigitComponent (Right n) = isDigitComponent n

joinMaybeEnter : List (Either CalcMacro CalcOp) -> List (Either CalcMacro CalcOp) -> List (Either CalcMacro CalcOp)
joinMaybeEnter leftOps rightOps@(d :: rest) = 
  leftOps ++
    (if 
         startsWithDigitComponent d 
      && (fromMaybe False $ endsWithDigitComponent <$> (last' leftOps))
     then 
       [Right ENTER]
     else
       []) 
  ++ rightOps
joinMaybeEnter leftOps [] = leftOps

mutual

  -- the program generates the ctx from within itself
  compileProgram : List (String, Nat) -> Program -> List (Either CalcMacro CalcOp)
  compileProgram sn (MkProgram ss) = ss >>= compileLine sn

  compilePreparedProgram : List (String, Nat) -> PreparedProgram -> List (Either CalcMacro CalcOp)
  compilePreparedProgram sn (MkPreparedProgram ass) = ass >>= ((compileStatement sn) . snd)

  compileLine : List (String, Nat) -> Line -> List (Either CalcMacro CalcOp)
  compileLine sn (MkLine n ss) = compileStatements sn ss

  compileStatements : List (String, Nat) -> Statements -> List (Either CalcMacro CalcOp)
  compileStatements sn (MkStatements s) = s >>= compileStatement sn

  compileStatement : List (String, Nat) -> Statement -> List (Either CalcMacro CalcOp)
  compileStatement sn END = [Right RFasS]
  compileStatement sn (FOR i e1 e2 so) = [Right NotImplemented] -- TODO
  compileStatement sn (GOSUB n) = [Left (MkCalcMacroBasicLine n)] ++ [Right mGSBlineX]
  compileStatement sn (GOTO  n) = [Left (MkCalcMacroBasicLine n)] ++ [Right mGTOlineX]
  -- how to invert the expression check?
  compileStatement sn (IF e (MkThenClauseNat n)) = [Left (MkCalcMacroBasicLine n)] ++ (Right <$> [CHS, STO_I]) ++ compileExpression sn e ++ [Right GTO_I]
  compileStatement sn (INPUT idl) = let (MkIdList ids) = idl in ids >>= (\i => [
   (Right (SF 9)),
   (Right (RFasS)),
   -- user inputs number and presses R/S
   (Right (CF 9)),
   (Left (MkCalcMacroStoId i))])
  compileStatement sn (LETtis i e) = compileExpression sn e ++ [Left (MkCalcMacroStoId i)] -- [STO ( fromMaybe 0 $ lookup i (dictIdRegister ctx))]
  compileStatement sn (NEXT idlist) = [Right NotImplemented] -- TODO
  compileStatement sn (ONIdGOSUB id ns) = [ Right Digit0
                                          , Right STO_I
                                          , Left (MkCalcMacroRclId id) 
                                          , Right INT
                                          ] ++ (ns >>= (\n =>
                                          [ Left (MkCalcMacroBasicLine n)
                                          , Right mONSpaghetti])) ++
                                          [ Right RCL_I
                                          , Right (TEST 0)
                                          , Right mGSBlineI
                                          ]
  compileStatement sn (ONIdGOTO id ns) = [ Right Digit0
                                         , Right STO_I
                                         , Left (MkCalcMacroRclId id)
                                         , Right INT
                                         ] ++ (ns >>= (\n =>
                                         [ Left (MkCalcMacroBasicLine n)
                                         , Right mONSpaghetti])) ++
                                         [ Right RCL_I
                                         , Right (TEST 0)
                                         , Right GTO_I
                                         ]

  compileStatement sn (PRINT pl) = compilePrintList sn pl
  compileStatement sn RETURN = [Right RTN]
  compileStatement sn (REM s) = []
  compileStatement sn x = idris_crash ("Can't compile statement: " ++ show x)

  compilePrintList : List (String, Nat) -> PrintList -> List (Either CalcMacro CalcOp)
  compilePrintList sn (MkPrintList mes) = (compileExpression sn <$> catMaybes mes) >>= (++ [Right mPRINT])

  compileExpression : List (String, Nat) -> Expression -> List (Either CalcMacro CalcOp)
  compileExpression sn (MkExpression (ande :: [])) = compileAndExp sn ande
  compileExpression _ _ = ?unsupported

  compileAndExp : List (String, Nat) -> AndExp -> List (Either CalcMacro CalcOp)
  compileAndExp sn (MkAndExp (ne :: [])) = compileNotExp sn ne
  compileAndExp _ _ = ?unsupported

  compileNotExp : List (String, Nat) -> NotExp -> List (Either CalcMacro CalcOp)
  compileNotExp sn (MkNotExp ce) = compileCompareExp sn ce
  compileNotExp _ _ = ?unsupported

  mkCCE : List (String, Nat) -> AddExp -> CompareExp -> CalcOp -> List (Either CalcMacro CalcOp)
  mkCCE sn ae ce op = (compileCompareExp sn ce) `joinMaybeEnter` (compileAddExp sn ae) ++ [Right $ op]

  -- TODO optimize by using the zero comparator functions if possible
  compileCompareExp : List (String, Nat) -> CompareExp -> List (Either CalcMacro CalcOp)
  compileCompareExp sn (MkCompareExpTis    ae ce) = mkCCE sn ae ce (TEST 5)
  compileCompareExp sn (MkCompareExpGalGar ae ce) = mkCCE sn ae ce (TEST 6)
  compileCompareExp sn (MkCompareExpGar    ae ce) = mkCCE sn ae ce (TEST 7)
  compileCompareExp sn (MkCompareExpGarTis ae ce) = mkCCE sn ae ce (TEST 9)
  compileCompareExp sn (MkCompareExpGal    ae ce) = mkCCE sn ae ce (TEST 8)
  compileCompareExp sn (MkCompareExpGalTis ae ce) = mkCCE sn ae ce XGalTisY
  compileCompareExp sn (MkCompareExp       ae   ) =   compileAddExp sn ae

  compileAddOperator : AddOperator -> List (Either CalcMacro CalcOp)
  compileAddOperator MkAddOperatorLus = [Right Lus]
  compileAddOperator MkAddOperatorHep = [Right Hep]

  compileAddExp : List (String, Nat) -> AddExp -> List (Either CalcMacro CalcOp)
  compileAddExp sn (MkAddExp multe laome) =
    foldl
      (\done, (addOp, me) => (
        (done `joinMaybeEnter`)
        (compileMultExp sn me) `joinMaybeEnter` 
        (compileAddOperator addOp)))
      (compileMultExp sn multe)
      laome

  compileMultOperator : MultOperator -> List (Either CalcMacro CalcOp)
  compileMultOperator MkMultOperatorTar = [Right Tar]
  compileMultOperator MkMultOperatorFas = [Right Fas]

  compileMultExp : List (String, Nat) -> MultExp -> List (Either CalcMacro CalcOp)
  compileMultExp sn (MkMultExp nege lmone) =
    foldl
      (\done, (multOp, ne) => 
        (done `joinMaybeEnter`)
        (compileNegateExp sn ne) `joinMaybeEnter` 
        (compileMultOperator multOp))
      (compileNegateExp sn nege)
      lmone

  compileNegateExp : List (String, Nat) -> NegateExp -> List (Either CalcMacro CalcOp)
  compileNegateExp sn (MkNegateExpHep pe) = (compilePowerExp sn pe) ++ [Right CHS]
  compileNegateExp sn (MkNegateExp pe) = compilePowerExp sn pe

  compilePowerExp : List (String, Nat) -> PowerExp -> List (Either CalcMacro CalcOp)
  compilePowerExp sn (MkPowerExp (sube :: subes)) =
    foldl
      (\done, next => (done `joinMaybeEnter` (compileSubExp sn next)) ++ (the (List (Either CalcMacro CalcOp)) (pure $ Right YKetX)))
      (compileSubExp sn sube)
      subes
  compilePowerExp sn (MkPowerExp []) = idris_crash "Empty PowerExp"

  compileSubExp : List (String, Nat) -> SubExp -> List (Either CalcMacro CalcOp)
  compileSubExp sn (MkSubExpPalPar e) = assert_total $ compileExpression sn e
  compileSubExp sn (MkSubExp v) = compileValue sn v

  mkCVE : List (String, Nat) -> Expression -> CalcOp -> List (Either CalcMacro CalcOp)
  mkCVE sn e op = assert_total $ (compileExpression sn e) ++ [Right op]
 
  compileValue : List (String, Nat) -> Value -> List (Either CalcMacro CalcOp)
  compileValue sn (MkValueId i) = [Left (MkCalcMacroRclId i)] -- RCL $ fromMaybe 666 $ lookup i (dictIdRegister ctx)] -- 666 is nonsense
  compileValue sn (MkValueConstant c) = compileConstant sn c
  compileValue sn (ABS e) = mkCVE sn e ABS
  compileValue sn (ASC e) = idris_crash "ASC not supported"
  compileValue sn (ATN e) = mkCVE sn e TANKetHep1
  compileValue sn (CHRbuc e) = [] -- TODO hm.
  compileValue sn (COS e) = mkCVE sn e COS
  compileValue sn (EXP e) = mkCVE sn e EKetX
  compileValue sn (FRE v) = idris_crash "FRE not supported"
  compileValue sn (INT e) = mkCVE sn e INT
  compileValue sn (LEFTbuc e e2) = assert_total $ compileExpression sn e  -- best effort. TODO test
  compileValue sn (LEN e) = idris_crash "LEN not supported. Could it?"
  compileValue sn (PEEK e) = assert_total $ (compileExpression sn e) ++ [Right mRCL_PalXPar]
                             {-
                             [ STO_I
                             , CLX
                             , RCL_PaliPar
                             ])
                             -}
  compileValue sn (POS v) = idris_crash "POS not supported. Should his be a NOP?"
  compileValue sn (RIGHTbuc e e2) = assert_total $ compileExpression sn e -- best effort.
  compileValue sn (RND e) = [Right RANHax]
  compileValue sn (SGN e) = idris_crash "SGN is currently not implemented, but could be. Just complain about this and it might get implemented."
  compileValue sn (SQR e) = mkCVE sn e SQRX
  compileValue sn (TAB e) = Right <$> [SF 8, Digit0] -- TODO will this work with PRINT?
  compileValue sn (TAN e) = mkCVE sn e TAN
  compileValue sn (VAL e) = idris_crash "VAL not supported"
  compileValue sn v = idris_crash $ "Compiling that Value is not supported: " ++ (show v) -- TODO should not be necessary? Why is it not total?

  numberComponentToOp : Char -> CalcOp
  numberComponentToOp '0' = Digit0
  numberComponentToOp '1' = Digit1
  numberComponentToOp '2' = Digit2
  numberComponentToOp '3' = Digit3
  numberComponentToOp '4' = Digit4
  numberComponentToOp '5' = Digit5
  numberComponentToOp '6' = Digit6
  numberComponentToOp '7' = Digit7
  numberComponentToOp '8' = Digit8
  numberComponentToOp '9' = Digit9
  numberComponentToOp '.' = Dot
  numberComponentToOp c   = idris_crash $ "Non-Number component in Number: " ++ show c

  compileNat : Nat -> List (Either CalcMacro CalcOp)
  compileNat n = Right <$> numberComponentToOp <$> unpack (show n)

  compileConstant : List (String, Nat) -> Constant -> List (Either CalcMacro CalcOp)
  compileConstant sn (MkConstantInteger n) = compileNat n 
  compileConstant sn (MkConstantString s) = Right (SF 8) :: compileNat (surely (lookup s sn))
  compileConstant sn (MkConstantReal s) = Right <$> numberComponentToOp <$> unpack s

  --prepareJmp : Nat -> CbCtx -> List (Either CalcMacro CalcOp)
  --prepareJmp n ctx = (compileNat $ lookupLine n ctx) ++ [CHS, STO_I]

ensureThreeDigits : List (Either CalcMacro CalcOp) -> List (Either CalcMacro CalcOp)
ensureThreeDigits ops = assert_total $ if length ops < 3 
                           then ensureThreeDigits (Right Digit0 :: ops)
                           else ops

expandMacro : CalcMacro -> CbCtx -> List CalcOp
expandMacro (MkCalcMacroBasicLine n) ctx = rights $ ensureThreeDigits $ compileNat $ lookupLine n ctx
expandMacro (MkCalcMacroStoId i) ctx = [STO $ lookupRegister i ctx]
expandMacro (MkCalcMacroRclId i) ctx = [RCL $ lookupRegister i ctx]

--implementation Compile Expression CbCtx (List (Either CalcMacro CalcOp)) where compile = compileExpression                                                         
--implementation Compile Constant CbCtx (List (Either CalcMacro CalcOp)) where compile = compileConstant

-- expression complexity
interface StackAnalytics a where
  maxStackReq : a -> Nat

--implementation StackAnalytics AddExpr where -- or
  --maxStackReq

-- example Basic P1=((Z-1)*P1+D*100/P)/Z
-- RCLZ 1 - RCLP1 * RCLD 100 * RCLP / + RCLZ /

record PreparedLine where 
  constructor MkPreparedLine
  basicLineNumber : Nat
  preparedCode : List (Either CalcMacro CalcOp)

formatFinishedCode : List CalcOp -> String
formatFinishedCode ops = joinWithList "\n" $ show <$> ops

--expandMacro : CalcMacro -> CbCtx -> List CalcOp -- [1,2,3,CHS,STO_I]
--expandMacro (MkCalcMacroBasicLine n) ctx = prepareJmp n ctx

interface OpCount a where
  opCount : a -> Nat


implementation OpCount CalcOp where
  opCount co = 1 -- it's already compiled so 1 CalcOp = 1 CalcOp

implementation OpCount CalcMacro where
  opCount = opCountCalcMacro

-- useful for Either CalcMacro CalcOp
implementation (OpCount l, OpCount r) => OpCount (Either l r) where
  opCount (Left  l) = opCount l
  opCount (Right r) = opCount r 

implementation OpCount PreparedLine where
  opCount (MkPreparedLine bln pc) = sum $ opCount <$> pc 



-- TODO compile to List (Either CalcMacro CalcOp) instead?

prepareLine : List (String, Nat) -> Line -> PreparedLine
prepareLine sn (MkLine n ss) = MkPreparedLine n $ compileStatements sn ss

constructLineMap : List (String, Nat) -> Program -> List (Nat, Nat)
constructLineMap   sn               (MkProgram l) = 
  let
    lines                            = (prepareLine sn <$> l) 
    initial : (List (Nat, Nat), Nat) = ([], length runtimeLibrary + 1)
  in
    fst $ foldl ( \(linemap, opc), (MkPreparedLine n pc) =>
      ((n,opc) :: linemap, opc +  (sum $ opCount <$> pc)))
      initial
      lines

constructTextMap : Program -> List (String, Nat)
constructTextMap p = let strings = nub $ selectStringProgram p in
                         zip strings (enumFromTo 1 $ length strings)

extractContext : List (String, Nat) -> Program -> PreparedProgram -> CbCtx 
extractContext   sn                    p          pp               =
  let
    ids : List Id = sort $ nub $ select p
    idToNat = zip ids (natRange (length ids))
  in
    MkCbCtx idToNat [] (constructLineMap sn p) [] -- TODO

expandEither : CbCtx -> Either CalcMacro CalcOp -> List CalcOp
expandEither ctx (Left m) = expandMacro m ctx
expandEither ctx (Right op) = [op]

expandMacros : CbCtx -> List (Either CalcMacro CalcOp) -> List CalcOp
expandMacros ctx c = c >>= expandEither ctx

{-
buildProgram : List (String, Nat) -> Program -> List CalcOp
buildProgram sn p = let ctx = extractContext sn p
                        objCode = compileProgram sn p in
                        runtimeLibrary ++ expandMacros ctx objCode
                        -}

buildPreparedProgram : List (String, Nat) -> Program -> List CalcOp
buildPreparedProgram   sn                    p =
  let
    pp = prepareProgram p
    ctx = extractContext sn p pp
    objCode = compilePreparedProgram sn pp
  in
    runtimeLibrary ++ expandMacros ctx objCode
