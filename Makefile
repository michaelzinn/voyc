clean:
	find . -name '*.ibc' | xargs rm -f
test:
	idris --testpkg voyc.ipkg
build:
	idris --build voyc.ipkg
